package id.co.hijr.oauth2.handler;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;


public class CustomLogoutHandler implements LogoutHandler {
	
	@Autowired
    private LoginMapper loginMapper;
	
	@Value("${app.cookie.name}")
	private String cookieName;
	
	@Value("${app.cookie.used}")
	protected String cookieUsed;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		// TODO Auto-generated method stub
		if(authentication != null) {
			Cookie cookie = new Cookie("JSESSIONID", null);
			String cookiePath = request.getContextPath();
			cookie.setPath(cookiePath);
			cookie.setMaxAge(-1);
			response.addCookie(cookie);
			
			
			if(cookieUsed.equals("true")) {
				Cookie cookie1 = new Cookie(cookieName, null);
				cookie1.setPath("/");
				cookie1.setMaxAge(-1);
				if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
					cookie1.setDomain(request.getServerName());
				}else {
					cookie1.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
				}
				response.addCookie(cookie1);
			}
			
			
			Account account = (Account) authentication.getPrincipal();
		
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Login.ACCOUNT_ID + "='"+account.getId()+"'");
			
			List<Login> lstLogin = loginMapper.getList(param);
			
	        for(Login login: lstLogin) {
	        		login.setStatus(Login.LOGOUT);
		        if(login.getExpireTime().before(new Date())) {
		        		login.setStatus(Login.EXPIRED);
	            }
		        loginMapper.insertArchive(login); // 1
	    			loginMapper.delete(login); // 2
	        }
	        
		}
	}

}
