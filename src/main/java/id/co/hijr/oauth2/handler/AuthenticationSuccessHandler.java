package id.co.hijr.oauth2.handler;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.WebUtils;
import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.services.HijrTokenService;


public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Value("${app.cookie.name}")
	private String cookieName;
	
	@Value("${app.cookie.used}")
	protected String cookieUsed;
	
	@Value("${security.oauth2.client.id}")
	private String clientId;
	
	@Value("${security.oauth2.resource.id}")
	private String resourceId;
	
	
	@Autowired
    private AuthorizationServerTokenServices tokenServices;
	
	
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(cookieUsed.equals("true")) {
			String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);
			HijrTokenService ts  = (HijrTokenService)tokenServices;
			if(ts.readAccessToken(accessTokenValue) == null || accessTokenValue.equals("")) {
				OAuth2AccessToken token = generateOauth2AccessToken(authentication);
				
				Cookie sessionCookie = new Cookie( cookieName, token.getValue() );
		        sessionCookie.setPath("/");
		        sessionCookie.setMaxAge(token.getExpiresIn());
		        if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
		        	sessionCookie.setDomain(request.getServerName());
				}else {
					sessionCookie.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
				}
		        response.addCookie( sessionCookie );
			}	
		}
			
		super.onAuthenticationSuccess(request, response, authentication);
	}
	
    private OAuth2AccessToken generateOauth2AccessToken(Authentication authentication) {
        Account user = (Account) authentication.getPrincipal();

        boolean approved = true;

        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();

        Map<String, String> requestParameters = new HashMap<String, String>();
        Map<String, Serializable> extensionProperties = new HashMap<String, Serializable>();
        Set<String> scope = new HashSet<>();
        scope.add(resourceId);

        Set<String> resourceIds = new HashSet<>();
        Set<String> responseTypes = new HashSet<>();
        responseTypes.add("code");

        OAuth2Request oAuth2request = new OAuth2Request(requestParameters, clientId, authorities, approved, scope, resourceIds, null, responseTypes, extensionProperties);

        OAuth2Authentication auth = new OAuth2Authentication(oAuth2request, authentication);

        HijrTokenService ts  = (HijrTokenService)tokenServices;
        return ts.createAccessToken(auth);
    }
	
}
