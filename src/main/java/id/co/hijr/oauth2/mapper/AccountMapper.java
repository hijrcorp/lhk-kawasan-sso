package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.QueryParameter;



@Mapper
public interface AccountMapper {
	
	@Insert("INSERT INTO hijr_account (id_account, username_account, password_account, email_account, email_status_account, first_name_account, last_name_account, mobile_account, mobile_status_account, male_status_account, birth_date_account, enabled_account, account_non_expired_account, credentials_non_expired_account, account_non_locked_account, person_added_account, time_added_account, person_modified_account, time_modified_account, login_account) VALUES (#{id:VARCHAR}, #{username:VARCHAR}, #{password:VARCHAR}, #{email:VARCHAR}, #{emailStatus:BOOLEAN}, #{firstName:VARCHAR}, #{lastName:VARCHAR}, #{mobile:VARCHAR}, #{mobileStatus:BOOLEAN}, #{maleStatus:BOOLEAN}, #{birthDate:DATE}, #{enabled:BOOLEAN}, #{accountNonExpired:BOOLEAN}, #{credentialsNonExpired:BOOLEAN}, #{accountNonLocked:BOOLEAN}, #{personAdded:VARCHAR}, #{timeAdded:TIMESTAMP}, #{personModified:VARCHAR}, #{timeModified:TIMESTAMP}, #{login:NUMERIC})")
	void insert(Account account);

	@Update("UPDATE hijr_account SET id_account=#{id:VARCHAR}, username_account=#{username:VARCHAR}, password_account=#{password:VARCHAR}, email_account=#{email:VARCHAR}, email_status_account=#{emailStatus:BOOLEAN}, first_name_account=#{firstName:VARCHAR}, last_name_account=#{lastName:VARCHAR}, mobile_account=#{mobile:VARCHAR}, mobile_status_account=#{mobileStatus:BOOLEAN}, male_status_account=#{maleStatus:BOOLEAN}, birth_date_account=#{birthDate:DATE}, enabled_account=#{enabled:BOOLEAN}, account_non_expired_account=#{accountNonExpired:BOOLEAN}, credentials_non_expired_account=#{credentialsNonExpired:BOOLEAN}, account_non_locked_account=#{accountNonLocked:BOOLEAN}, person_added_account=#{personAdded:VARCHAR}, time_added_account=#{timeAdded:TIMESTAMP}, person_modified_account=#{personModified:VARCHAR}, time_modified_account=#{timeModified:TIMESTAMP}, login_account=#{login:NUMERIC} WHERE id_account=#{id}")
	void update(Account account);

	@Delete("DELETE FROM hijr_account WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_account WHERE id_account=#{id}")
	void delete(Account account);

	List<Account> getList(QueryParameter param);

	Account getEntity(String id);

	long getCount(QueryParameter param);
	

	/**********************************************************************/
	
	@Update("UPDATE hijr_account SET login_account=ifnull(login_account,0)+1 WHERE id_account=#{id}")
	void updateLogin(Account account);
}
