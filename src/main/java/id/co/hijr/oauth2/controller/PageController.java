package id.co.hijr.oauth2.controller;

import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import id.co.hijr.oauth2.WebUtils;
import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.mapper.AccountMapper;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.ref.AccountLoginInfo;


@Controller
public class PageController {
	
	@Value("${app.cookie.name}")
	private String cookieName;
	
	@Value("${app.cookie.used}")
	protected String cookieUsed;
	
	@Value("${user.picture.url}")
    private String pictureUrl;
	
	@Value("${main.application.url}")
    private String mainApplicationUrl;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Autowired
    private AccountLoginMapper accountLoginMapper;
	
	@Autowired
    private AccountMapper accountMapper;
	

	@RequestMapping("/login")
    public String login(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		return loadIndex(model, request, response);
    }
	
	@RequestMapping(value="/verify", method = RequestMethod.POST)
    public String verify(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		String userId = request.getParameter("username");

		if(userId != null) {
			System.out.println("Mencoba masuk : "+userId);
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");
//			param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");
//			param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
			param.setClause(param.getClause() + ")");
			List<AccountLogin> lst = accountLoginMapper.getList(param);
			if(lst.size() > 0) {
				AccountLogin accountLogin = lst.get(0);
				boolean verify = accountLogin.isEnabled();
				
				model.addAttribute("verify", verify);
				if(verify) {
					model.addAttribute("action", request.getContextPath() + "/login");
				}else {
					model.addAttribute("action", request.getContextPath() + "/daftar");
				}
				model.addAttribute("realName", accountLogin.getFullName());
				model.addAttribute("cookieName", cookieName);
				model.addAttribute("userName", userId);
				model.addAttribute("email", accountLogin.getEmail());
				model.addAttribute("mobile", accountLogin.getMobile());
				model.addAttribute("picture", pictureUrl+accountLogin.getId());
				if(lst.size() > 1) {
					model.addAttribute("listSrc", lst);
				}else {
					model.addAttribute("source", accountLogin.getSource().getId());
				}
				
			}else {
				response.sendRedirect(request.getContextPath() + "/login?error");
			}
		}else {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		
		
		
		return "index";
    }
	
	
	@RequestMapping(value="/password", method = RequestMethod.GET)
    public String password(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
			
			model.addAttribute("verify", false);
			model.addAttribute("cookieName", cookieName);
			model.addAttribute("action", request.getContextPath() + "/update");
			model.addAttribute("realName", accountLogin.getFullName());
			model.addAttribute("userName", accountLogin.getUsername());
			model.addAttribute("email", accountLogin.getEmail());
			model.addAttribute("mobile", accountLogin.getMobile());
			model.addAttribute("picture", pictureUrl+accountLogin.getId());
			model.addAttribute("source", accountLogin.getSource().getId());
		} catch (Exception e) {
			// TODO: handle exception
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		
		
		
		return "index";
    }
	
	@RequestMapping(value="/daftar", method = RequestMethod.POST)
    public String daftar(Model model,
    			@RequestParam("email") Optional<String> email,
    			@RequestParam("mobile") Optional<String> mobile,
    			HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		System.out.println("register");
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if(username != null && password != null && !password.equals("")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND (" + AccountLogin.USERNAME + "='"+username+"')");
			List<AccountLogin> lst = accountLoginMapper.getList(param);
			if(lst.size() > 0) {
				AccountLogin account = lst.get(0);
				account.setPassword(passwordEncoder.encode(password));
				if(email.isPresent()) account.setEmail(email.get());
				if(mobile.isPresent()) account.setMobile(mobile.get());
				account.setEnabled(true);
				accountMapper.update(account);
			}else {
				response.sendRedirect(request.getContextPath() + "/login?error");
			}
			
			response.sendRedirect(request.getContextPath() + "/login?register");
		}else {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		
		
		return "index";
    }
	
	@RequestMapping(value="/update", method = RequestMethod.POST)
    public String update(Model model,
    			@RequestParam("email") Optional<String> email,
    			@RequestParam("mobile") Optional<String> mobile,
    			HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		System.out.println("update");
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
		
		System.out.println(accountLogin.getId());
		String password = request.getParameter("password");
		
		if(password != null && !password.equals("")) {
			
			AccountLogin account = accountLoginMapper.getEntity(accountLogin.getId()+accountLogin.getSource().getId());
			account.setPassword(passwordEncoder.encode(password));
			if(email.isPresent()) account.setEmail(email.get());
			if(mobile.isPresent()) account.setMobile(mobile.get());
			account.setEnabled(true);
			accountMapper.update(account);
			
			response.sendRedirect(request.getContextPath() + "/login?password");
		}else {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		
		
		return "index";
    }
	
	@RequestMapping("/refresh")
    public void refresh(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
	
		Cookie cookie1 = new Cookie(cookieName, null);
		cookie1.setMaxAge(-1);
		cookie1.setPath("/");
		response.addCookie(cookie1);
		response.sendRedirect(request.getContextPath());

    }
	
	@RequestMapping("/oauth/error")
    public String oauthError()throws Exception {
		return "error";
    }
	
	@RequestMapping("/")
    public String index(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		return loadIndex(model, request, response);
    }
	
	protected String extractHeaderToken(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
			String value = headers.nextElement();
			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
				// Add this here for the auth details later. Would be better to change the signature of this method.
				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE,
						value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
				int commaIndex = authHeaderValue.indexOf(',');
				if (commaIndex > 0) {
					authHeaderValue = authHeaderValue.substring(0, commaIndex);
				}
				return authHeaderValue;
			}
		}

		return "";
	}
	
	public static String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				if (name.equals(cookieName)) {
					accessTokenValue = value;
				}
			}
		}

		return accessTokenValue;
	}
	
	private String loadIndex(Model model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
			
			model.addAttribute("picture", pictureUrl+accountLogin.getId());
			response.sendRedirect(mainApplicationUrl);
		} catch (Exception exception){
		    //Invalid token
			//exception.printStackTrace();
			
			//System.out.println("belum login");
			
			if(cookieUsed.equals("true")) {
				String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);

				if (!accessTokenValue.equals("")) {
					
					try {
					    DecodedJWT jwt = JWT.decode(accessTokenValue);
					    
					    model.addAttribute("verify", true);
					    model.addAttribute("cookieName", cookieName);
					    model.addAttribute("action", request.getContextPath() + "/login");
					    model.addAttribute("userName", jwt.getClaim("account_id").asString());
						model.addAttribute("realName",jwt.getClaim("full_name").asString());
						model.addAttribute("organization", jwt.getClaim("organization_id").asString());
						model.addAttribute("picture", jwt.getClaim("account_avatar").asString());
						
						
					} catch (JWTDecodeException e){
					    //Invalid token
						e.printStackTrace();
					}
				}
			}
			model.addAttribute("mainApp", mainApplicationUrl);
		}

		return "index";
	}
}
