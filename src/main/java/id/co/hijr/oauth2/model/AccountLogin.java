package id.co.hijr.oauth2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountLogin extends Account {
	
	public final static String FULL_NAME = "full_name_account";
	public final static String ADMIN = "admin_account";
	
	private Source source;
	private String fullName;
	private boolean admin;

	@JsonProperty("full_name")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}


}
